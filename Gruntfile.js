module.exports = function(grunt) {
    var pathDelpoy = 'deploy',
        pathBuild = 'build';

    // 1. Вся настройка находится здесь
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        bower_concat: {
            all: {
                dest: pathDelpoy+'/js/libs/_bower.js' // Клеем все Bower-файлы
            }
        },

        concat: {
            dist: {
                src: [
                    pathDelpoy+'/js/libs/_bower.js', // Берем Bower-файл
                    pathDelpoy+'/custom/js/*.js'  // Берем все кастомные js
                ],
                dest: pathBuild+'/js/scripts.js' // Клеим все
            }
        },

        uglify: {
            build: {
                src: pathBuild+'/js/scripts.js',
                dest: pathBuild+'/js/scripts.min.js'
            }
        },

        watch: {
            scripts: {
                files: [pathDelpoy+'/custom/*.js'],
                tasks: ['bower_concat', 'concat', 'uglify'],
                options: {
                    spawn: false
                }
            }
        }
    });

    // 3. Тут мы указываем Grunt, что хотим использовать этот плагин
    grunt.loadNpmTasks('grunt-bower-concat');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // 4. Указываем, какие задачи выполняются, когда мы вводим «grunt» в терминале
    grunt.registerTask('default', ['bower_concat', 'concat', 'uglify']);

};